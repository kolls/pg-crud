.PHONY: default
default: test;

SHA := $(shell git rev-parse --short=8 HEAD)
GITVERSION := $(shell git describe --long --all)
BUILDDATE := $(shell date -Iseconds)
VERSION := $(or ${VERSION},devel)
LDFLAGS := \
	-X 'gitlab.com/kolls/pg-crud/version.Version=$(VERSION)' \
	-X 'gitlab.com/kolls/pg-crud/version.Revision=$(GITVERSION)' \
	-X 'gitlab.com/kolls/pg-crud/version.GitSHA1=$(SHA)' \
	-X 'gitlab.com/kolls/pg-crud/version.BuildDate=$(BUILDDATE)'

.PHONY: gofmt
gofmt:
	GO111MODULE=off go fmt ./...

.PHONY: test
test: gofmt
	go test -race -coverprofile cover.out ./... && go tool cover -func cover.out

.PHONY: run
run: gofmt
	go run main.go

.PHONY: golint
golint:
	golangci-lint run

.PHONY: pg-crud
pg-crud:
	go mod download
	go mod tidy
	go build \
		-tags netgo \
		-ldflags \
		"$(LDFLAGS)" \
		-o bin/pg-crud

