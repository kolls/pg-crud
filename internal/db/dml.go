package db

import (
	"fmt"
	"strings"
)

func createUpsertSQL(entity *entity) string {
	placeholders := make([]string, len(entity.cols))
	cols := make([]string, len(placeholders))
	var setters []string
	var pk string
	for i, c := range entity.cols {
		placeholders[i] = fmt.Sprintf("$%d", i+1)
		cols[i] = c.name
		if c.primaryKey {
			pk = c.name
		} else {
			setters = append(setters, fmt.Sprintf("%s=%s", c.name, placeholders[i]))
		}
	}
	return fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s) ON CONFLICT (%s) DO UPDATE SET %s", entity.table, strings.Join(cols, ", "), strings.Join(placeholders, ", "), pk, strings.Join(setters, ", "))
}
