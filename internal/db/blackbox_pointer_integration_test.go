package db_test

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/rs/xid"
	"github.com/stretchr/testify/require"
	"gitlab.com/kolls/pg-crud/pkg/config"
	"gitlab.com/kolls/pg-crud/pkg/pg"
	"gitlab.com/kolls/pg-crud/pkg/test"
	"go.uber.org/zap"
	"os"
	"path/filepath"
	"testing"
	"time"
)

type Gender = int

const (
	Male Gender = iota + 1
	Female
)

type UserPtr struct {
	UUID        *string
	Gender      *Gender
	FirstName   *string
	LastName    *string
	Birthdate   *time.Time
	Email       *string
	HasChildren *bool
	Addresses   *[]*AddressPtr `db:"address_ids"`
	Mix         *MixPtr        `db:"mix_id"`
	private     *bool
	Private     *bool `db:"-"`
}

type AddressPtr struct {
	Street       *string
	City         *string
	PhoneNumbers *[]*int32
	ID           *int
}

type MixPtr struct {
	Letter   *rune   `db:"my_letter"`
	MixID    *uint16 `db:"pk"`
	Byte     *byte
	Matrix   *[][]*int16
	Matrix3D *[][][]*int64
	Float32  *float32
	Float64  *float64
	Binary   *[]byte
	Uint     *uint
	Uint8    *uint8
	Uint16   *uint16
	Uint32   *uint32
	Uint64   *uint64
	Map      *map[*string]*string
	Impl     *ImplPtr
}

type ImplPtr struct {
	X *string
}

func TestModelPtrInContainer(t *testing.T) {
	workingDir, err := os.Getwd()
	require.Nil(t, err)
	dataDir := filepath.Join(workingDir, "data-pg-test-model-pointer")
	testDB, err := pg.Run(dataDir, "test-model-pointer", "postgres", "postgres", 54442, "pg-test-model-pointer", zap.NewExample(), config.DropTables())
	require.Nil(t, err)
	defer testDB.Shutdown()
	testModelPtr(t, testDB)
}

func aTestModelPtr(t *testing.T) {
	testModelPtr(t, test.MockDB(t, zap.NewExample()))
}

func testModelPtr(t *testing.T, db pg.Database) {
	ctx := context.Background()
	birthdate := time.Date(2000, 1, 1, 18, 30, 0, 0, time.UTC)
	binary := []byte("Some binary data")
	m := map[*string]*string{stringPtr("b"): stringPtr("B")}
	doeAddress := &AddressPtr{
		ID:           intPtr(1),
		Street:       stringPtr("Sixth Street"),
		City:         stringPtr("Austin"),
		PhoneNumbers: &[]*int32{int32Ptr(111)},
	}
	doe := &UserPtr{
		UUID:        stringPtr(xid.New().String()),
		Gender:      intPtr(Male),
		FirstName:   stringPtr("John"),
		LastName:    stringPtr("Doe"),
		Birthdate:   &birthdate,
		Email:       stringPtr("john.doe@gmail.com"),
		HasChildren: boolPtr(false),
		Addresses:   &[]*AddressPtr{doeAddress},
		Mix: &MixPtr{
			MixID:    uint16Ptr(2),
			Letter:   runePtr('b'),
			Byte:     bytePtr('B'),
			Matrix:   &[][]*int16{{int16Ptr(1), int16Ptr(0), int16Ptr(0)}, {int16Ptr(0), int16Ptr(1), int16Ptr(0)}},
			Matrix3D: &[][][]*int64{{{int64Ptr(1), int64Ptr(0), int64Ptr(0)}, {int64Ptr(0), int64Ptr(1), int64Ptr(0)}}},
			Float32:  float32Ptr(2),
			Float64:  float64Ptr(3.1415),
			Binary:   &binary,
			Uint:     uintPtr(4),
			Uint8:    uint8Ptr(8),
			Uint16:   uint16Ptr(16),
			Uint32:   uint32Ptr(32),
			Uint64:   uint64Ptr(64),
			Map:      &m,
			Impl:     &ImplPtr{X: stringPtr("x")},
		},
	}

	usr := new(UserPtr)
	err := db.LoadSingle(ctx, doe.UUID, usr)
	require.NotNil(t, err)

	err = db.Store(ctx, doe)
	require.Nil(t, err)

	err = db.LoadSingle(ctx, doe.UUID, usr)
	require.Nil(t, err)
	requireMapEqual(t, doe.Mix.Map, usr.Mix.Map)
	usr.Mix.Map = doe.Mix.Map
	require.Equal(t, doe, usr)

	addr := new(AddressPtr)
	err = db.LoadSingle(ctx, doeAddress.ID, addr)
	require.Nil(t, err)
	require.Equal(t, doeAddress, addr)

	lennon := &UserPtr{
		UUID:      stringPtr(xid.New().String()),
		Gender:    intPtr(Male),
		FirstName: stringPtr("John"),
		LastName:  stringPtr("Lennon"),
		Email:     stringPtr("john.lennon@gmail.com"),
		Addresses: doe.Addresses,
		Mix:       nil, // intentionally
	}
	err = db.Store(ctx, lennon)
	require.Nil(t, err)

	pt := &UserPtr{
		UUID: doe.UUID,
	}
	usr = new(UserPtr)
	err = db.LoadSingle(ctx, pt, usr)
	require.Nil(t, err)
	requireMapEqual(t, doe.Mix.Map, usr.Mix.Map)
	usr.Mix.Map = doe.Mix.Map
	require.Equal(t, doe, usr)

	pt = &UserPtr{
		FirstName: doe.FirstName,
		LastName:  doe.LastName,
	}
	usr = new(UserPtr)
	err = db.LoadSingle(ctx, pt, usr)
	require.Nil(t, err)
	requireMapEqual(t, doe.Mix.Map, usr.Mix.Map)
	usr.Mix.Map = doe.Mix.Map
	require.Equal(t, doe, usr)

	pt = &UserPtr{
		FirstName: doe.FirstName,
		LastName:  doe.LastName,
	}
	err = db.Complete(ctx, pt)
	require.Nil(t, err)
	requireMapEqual(t, doe.Mix.Map, pt.Mix.Map)
	pt.Mix.Map = doe.Mix.Map
	require.Equal(t, doe, pt)

	pt = &UserPtr{
		FirstName: doe.FirstName,
	}
	err = db.Complete(ctx, pt)
	require.Nil(t, err)
	requireMapEqual(t, doe.Mix.Map, pt.Mix.Map)
	pt.Mix.Map = doe.Mix.Map
	require.Equal(t, doe, pt)

	pt = &UserPtr{
		FirstName: doe.FirstName,
		LastName:  lennon.LastName,
	}
	err = db.Complete(ctx, pt)
	require.Nil(t, err)
	require.Equal(t, lennon, pt)

	pt = &UserPtr{
		LastName: stringPtr(*doe.LastName + "XXX"),
	}
	err = db.Complete(ctx, pt)
	require.Equal(t, pgx.ErrNoRows, err)

	pt = &UserPtr{
		Addresses: &[]*AddressPtr{addr},
	}
	err = db.Complete(ctx, pt)
	require.Nil(t, err)
	requireMapEqual(t, doe.Mix.Map, pt.Mix.Map)
	pt.Mix.Map = doe.Mix.Map
	require.Equal(t, doe, pt)

	doe.Addresses = nil
	err = db.Store(ctx, doe)
	require.Nil(t, err)

	pt = &UserPtr{
		Addresses: &[]*AddressPtr{addr},
	}
	err = db.Complete(ctx, pt)
	require.Nil(t, err)
	require.Equal(t, lennon, pt)

	usr2 := UserPtr{
		UUID: doe.UUID,
	}
	err = db.Delete(ctx, usr2)
	require.Nil(t, err)

	usr = new(UserPtr)
	err = db.LoadSingle(ctx, doe.UUID, usr)
	require.NotNil(t, err)
}

func requireMapEqual(t *testing.T, exp, act *map[*string]*string) {
	require.True(t, (exp == nil && act == nil) || (exp != nil && act != nil))
	if exp == nil {
		return
	}
	for ek, ev := range *exp {
		isEqual := false
		for ak, av := range *act {
			if *ek == *ak {
				isEqual = *ev == *av
				break
			}
		}
		require.True(t, isEqual)
	}
}

func stringPtr(s string) *string {
	return &s
}

func boolPtr(b bool) *bool {
	return &b
}

func runePtr(r rune) *rune {
	return &r
}

func bytePtr(b byte) *byte {
	return &b
}

func intPtr(i int) *int {
	return &i
}

func int16Ptr(i int16) *int16 {
	return &i
}

func int32Ptr(i int32) *int32 {
	return &i
}

func int64Ptr(i int64) *int64 {
	return &i
}

func uintPtr(i uint) *uint {
	return &i
}

func uint8Ptr(i uint8) *uint8 {
	return &i
}

func uint16Ptr(i uint16) *uint16 {
	return &i
}

func uint32Ptr(i uint32) *uint32 {
	return &i
}

func uint64Ptr(i uint64) *uint64 {
	return &i
}

func float32Ptr(f float32) *float32 {
	return &f
}

func float64Ptr(f float64) *float64 {
	return &f
}
