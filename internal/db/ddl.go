package db

import (
	"fmt"
	"strings"
)

func (db *database) createSchemaDDL(schema string) string {
	return fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s", schema)
}

func (db *database) createTableDDL(entity *entity) (string, error) {
	cc := make([]string, len(entity.cols))
	for i, c := range entity.cols {
		typ := c.typ
		pk := ""
		if c.primaryKey {
			pk = " PRIMARY KEY"
		}
		fk := ""
		if c.foreignKey != nil {
			fkColumn := ""
			for _, f := range c.foreignKey.cols {
				if f.primaryKey {
					fkColumn = f.name
					if !strings.Contains(c.typ, "[]") {
						typ = f.typ
					}
					break
				}
			}
			if fkColumn == "" {
				return "", fmt.Errorf("no primary key specified for entity %s", c.foreignKey.table)
			}
			if !strings.Contains(c.typ, "[]") {
				fk = fmt.Sprintf(" REFERENCES %s (%s) ON DELETE CASCADE", c.foreignKey.table, fkColumn)
			}
		}
		cc[i] = fmt.Sprintf("%s %s%s%s", c.name, typ, pk, fk)
	}
	return fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (%s)", entity.table, strings.Join(cc, ", ")), nil
}

func (db *database) dropTableDDL(entity *entity) string {
	return fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", entity.table)
}
