package db_test

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/rs/xid"
	"github.com/stretchr/testify/require"
	"gitlab.com/kolls/pg-crud/pkg/config"
	"gitlab.com/kolls/pg-crud/pkg/pg"
	"gitlab.com/kolls/pg-crud/pkg/test"
	"go.uber.org/zap"
	"image/color"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

type Salutation int

const (
	Mr Salutation = iota
	Miss
	Ms
	Mrs
)

// TODO type Colors []Color; type Color string -> Colors is not recognized as array: results in text and not text[]
// TODO: []...Struct, []...Interface, map[Struct][]...Struct, map[Interface][]...Interface
type User struct {
	UID           string
	Salutation    Salutation
	FirstName     string
	LastName      string
	Birthdate     time.Time
	Email         string
	FavoriteColor color.Color
	HasChildren   bool
	Addresses     []Address `db:"address_ids"`
	Mix           Mix       `db:"mix_id"`
	private       string
	Private       string `db:"-"`
	Embedded      []embedded
}

type Address struct {
	Street       string
	City         string
	PhoneNumbers []string
	ID           string
}

type Mix struct {
	Letter   rune   `db:"my_letter"`
	MixID    uint32 `db:"pk"`
	Byte     byte
	Matrix   [][]int32
	Matrix3D [][][]int8
	Float32  float32
	Float64  float64
	Binary   []byte
	Uint     uint
	Uint8    uint8
	Uint16   uint16
	Uint32   uint32
	Uint64   uint64
	Map      map[string]string
	Impl     Interface
}

type Interface interface {
	Get() int
}

type Impl struct {
	X int
}

type embedded struct {
	L string
}

func (i *Impl) Get() int {
	return i.X
}

func TestModelInContainer(t *testing.T) {
	workingDir, err := os.Getwd()
	require.Nil(t, err)
	dataDir := filepath.Join(workingDir, "data-pg-test-model")
	testDB, err := pg.Run(dataDir, "test-model", "postgres", "postgres", 54441, "pg-test-model", zap.NewExample(), config.CaseSensitive(), config.DoNotReplaceDotsWithUnderscores(), config.DropTables())
	require.Nil(t, err)
	defer testDB.Shutdown()
	testModel(t, testDB)
}

func aTestModel(t *testing.T) {
	mockDB := test.MockDB(t, zap.NewExample(),
		test.CaseSensitive(false),
		test.ReplaceDotsWithUnderscores(true),
		test.OnQueryRow(func(sql string, args ...interface{}) []interface{} {
			println(sql)
			if strings.Contains(sql, "user") {
				return []interface{}{
					"1234",
					Mr,
					"John",
					"Doe",
					time.Date(2000, 1, 1, 18, 30, 0, 0, time.UTC),
					"john.doe@gmail.com",
					&color.RGBA{R: 0, G: 255, B: 0, A: 1},
					true,
					[]Address{
						{
							ID:           xid.New().String(),
							Street:       "Sixth Street",
							City:         "Austin",
							PhoneNumbers: []string{"111", "222", "333"},
						},
					},
					Mix{
						MixID:    1,
						Letter:   'a',
						Byte:     'A',
						Matrix:   [][]int32{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}},
						Matrix3D: [][][]int8{{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {2, 0, 0}, {0, 2, 0}, {0, 0, 2}}},
						Float32:  float32(2),
						Float64:  3.1415,
						Binary:   []byte("Some binary data"),
						Uint:     uint(4),
						Uint8:    uint8(8),
						Uint16:   uint16(16),
						Uint32:   uint32(32),
						Uint64:   uint64(64),
						Map:      map[string]string{"a": "A"},
						Impl:     &Impl{X: 24},
					},
				}
			} else {

			}
			return nil
		}),
		test.Expect(t).Store(nil).Option(),
	)
	defer mockDB.AssertExpectations()
	testModel(t, mockDB)
}

func testModel(t *testing.T, db pg.Database) {
	ctx := context.Background()
	doeAddress := Address{
		ID:           xid.New().String(),
		Street:       "Sixth Street",
		City:         "Austin",
		PhoneNumbers: []string{"111", "222", "333"},
	}
	doe := &User{
		UID:           xid.New().String(),
		Salutation:    Mr,
		FirstName:     "John",
		LastName:      "Doe",
		Birthdate:     time.Date(2000, 1, 1, 18, 30, 0, 0, time.UTC),
		Email:         "john.doe@gmail.com",
		FavoriteColor: &color.RGBA{0, 255, 0, 1},
		HasChildren:   true,
		Addresses:     []Address{doeAddress},
		Mix: Mix{
			MixID:    1,
			Letter:   'a',
			Byte:     'A',
			Matrix:   [][]int32{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}},
			Matrix3D: [][][]int8{{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {2, 0, 0}, {0, 2, 0}, {0, 0, 2}}},
			Float32:  float32(2),
			Float64:  3.1415,
			Binary:   []byte("Some binary data"),
			Uint:     uint(4),
			Uint8:    uint8(8),
			Uint16:   uint16(16),
			Uint32:   uint32(32),
			Uint64:   uint64(64),
			Map:      map[string]string{"a": "A"},
			Impl:     &Impl{X: 24},
		},
		Embedded: []embedded{{L: "doe"}},
	}

	usr := new(User)
	err := db.LoadSingle(ctx, doe.UID, usr)
	require.NotNil(t, err)

	tx, err := db.Begin(ctx)
	require.Nil(t, err)

	err = db.Store(ctx, doe)
	require.Nil(t, err)

	err = tx.Rollback(ctx)
	require.Nil(t, err)

	usr = new(User)
	err = db.LoadSingle(ctx, doe.UID, usr)
	require.NotNil(t, err)

	tx, err = db.Begin(ctx)
	require.Nil(t, err)

	err = db.Store(ctx, doe)
	require.Nil(t, err)

	err = tx.Commit(ctx)
	require.Nil(t, err)

	db.RegisterConcreteTypes(User{
		FavoriteColor: &color.RGBA{},
		Mix: Mix{
			Impl: &Impl{},
		},
	})

	err = db.LoadSingle(ctx, doe.UID, usr)
	require.Nil(t, err)
	require.Equal(t, doe, usr)

	addr := new(Address)
	err = db.LoadSingle(ctx, doeAddress.ID, addr)
	require.Nil(t, err)
	require.Equal(t, doeAddress, *addr)

	lennon := &User{
		UID:         xid.New().String(),
		Salutation:  Mr,
		FirstName:   "John",
		LastName:    "Lennon",
		Email:       "john.lennon@gmail.com",
		Addresses:   doe.Addresses,
		Mix:         Mix{}, // intentionally,
		HasChildren: false,
		Embedded:    []embedded{{L: "lennon"}},
	}
	err = db.Store(ctx, lennon)
	require.Nil(t, err)

	prototype := &User{
		UID: doe.UID,
	}
	usr = new(User)
	err = db.LoadSingle(ctx, prototype, usr)
	require.Nil(t, err)
	require.Equal(t, doe, usr)

	prototype = &User{
		FirstName: doe.FirstName,
		LastName:  doe.LastName,
	}
	usr = new(User)
	err = db.LoadSingle(ctx, prototype, usr)
	require.Nil(t, err)
	require.Equal(t, doe, usr)

	prototype = &User{
		FirstName: doe.FirstName,
		LastName:  doe.LastName,
	}
	err = db.Complete(ctx, prototype)
	require.Nil(t, err)
	require.Equal(t, doe, prototype)

	prototype = &User{
		FirstName: doe.FirstName,
	}
	err = db.Complete(ctx, prototype)
	require.Nil(t, err)
	require.Equal(t, doe, prototype)

	prototype = &User{
		FirstName: doe.FirstName,
		LastName:  lennon.LastName,
	}
	err = db.Complete(ctx, prototype)
	require.Nil(t, err)
	require.Equal(t, lennon, prototype)

	prototype = &User{
		LastName: doe.LastName + "XXX",
	}
	err = db.Complete(ctx, prototype)
	require.Equal(t, pgx.ErrNoRows, err)

	prototype = &User{
		Addresses: []Address{*addr},
	}
	err = db.Complete(ctx, prototype)
	require.Nil(t, err)
	require.Equal(t, doe, prototype)

	doe.Addresses = nil
	err = db.Store(ctx, doe)
	require.Nil(t, err)

	prototype = &User{
		Addresses: []Address{*addr},
	}
	err = db.Complete(ctx, prototype)
	require.Nil(t, err)
	require.Equal(t, lennon, prototype)

	var uu []User
	err = db.Load(ctx, nil, true, false, &uu)
	require.Nil(t, err)
	require.Equal(t, 2, len(uu))
	require.Equal(t, *lennon, uu[0])
	require.Equal(t, *doe, uu[1])

	prototype = new(User)
	uu = uu[:0]
	err = db.Load(ctx, prototype, true, false, &uu)
	require.Nil(t, err)
	require.Equal(t, 2, len(uu))
	require.Equal(t, *lennon, uu[0])
	require.Equal(t, *doe, uu[1])

	prototype = &User{
		UID: doe.UID,
	}
	uu = uu[:0]
	err = db.Load(ctx, prototype, true, false, &uu)
	require.Nil(t, err)
	require.Equal(t, 1, len(uu))
	require.Equal(t, *doe, uu[0])

	prototype = &User{
		Addresses: []Address{*addr},
	}
	uu = uu[:0]
	err = db.Load(ctx, prototype, true, false, &uu)
	require.Nil(t, err)
	require.Equal(t, 1, len(uu))
	require.Equal(t, *lennon, uu[0])

	uu = uu[:0]
	err = db.LoadByQuery(ctx, &uu, "select * from db_test.\"User\"")
	require.Nil(t, err)
	require.Equal(t, 2, len(uu))
	require.Equal(t, *lennon, uu[0])
	require.Equal(t, *doe, uu[1])

	u2 := User{}
	u2.UID = "not-considered-by-next-call"
	err = db.LoadSingleByQuery(ctx, &u2, "select * from db_test.\"User\" where uid=$1", doe.UID)
	require.Nil(t, err)
	require.Equal(t, *doe, u2)

	usr2 := User{
		UID: doe.UID,
	}
	err = db.Delete(ctx, usr2)
	require.Nil(t, err)

	usr = new(User)
	err = db.LoadSingle(ctx, doe.UID, usr)
	require.NotNil(t, err)
}
