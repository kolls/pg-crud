package db

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"gitlab.com/kolls/pg-crud/pkg/config"
	"go.uber.org/zap"
	"reflect"
	"regexp"
	"strings"
	"time"
	"unicode"
)

type tag struct {
	col        string
	primaryKey bool
}

type database struct {
	*zap.Logger
	cfg       *config.Config
	querier   pgxtype.Querier
	cancel    context.CancelFunc
	implTypes map[string]reflect.Value
	reg       map[string]*entity
	jm        *JsonMarshaller
	mm        *MapMarshaller
}

type entity struct {
	typ    reflect.Type
	schema string
	table  string
	cols   []*column
}

type column struct {
	index      int
	name       string
	typ        string
	primaryKey bool
	foreignKey *entity
}

func Connect(ctx context.Context, url string, logger *zap.Logger, opts ...config.Option) (*database, error) {
	cfg := config.Default()
	for _, opt := range opts {
		opt(cfg)
	}

	c, cancel := context.WithCancel(ctx)

	conn, err := pgx.Connect(c, url)
	if err != nil {
		cancel()
		return nil, err
	}

	err = conn.Ping(c)
	if err != nil {
		cancel()
		return nil, err
	}

	db := create(conn, logger, cfg)
	db.cancel = cancel

	return db, nil
}

func New(querier pgxtype.Querier, logger *zap.Logger, opts ...config.Option) *database {
	cfg := config.Default()
	for _, opt := range opts {
		opt(cfg)
	}
	return create(querier, logger, cfg)
}

func create(querier pgxtype.Querier, logger *zap.Logger, cfg *config.Config) *database {
	return &database{
		cfg:       cfg,
		querier:   querier,
		Logger:    logger,
		implTypes: make(map[string]reflect.Value),
		reg:       make(map[string]*entity),
		jm:        new(JsonMarshaller),
		mm:        new(MapMarshaller),
	}
}

func (db *database) Opt(opt config.Option) {
	opt(db.cfg)
}

func (db *database) Begin(ctx context.Context) (pgx.Tx, error) {
	conn, ok := db.querier.(*pgx.Conn)
	if !ok {
		return nil, nil
	}
	return conn.Begin(ctx)
}

func (db *database) BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error) {
	conn, ok := db.querier.(*pgx.Conn)
	if !ok {
		return nil, nil
	}
	return conn.BeginTx(ctx, txOptions)
}

func (db *database) BeginFunc(ctx context.Context, f func(pgx.Tx) error) error {
	conn, ok := db.querier.(*pgx.Conn)
	if !ok {
		return nil
	}
	return conn.BeginFunc(ctx, f)
}

func (db *database) BeginTxFunc(ctx context.Context, txOptions pgx.TxOptions, f func(pgx.Tx) error) error {
	conn, ok := db.querier.(*pgx.Conn)
	if !ok {
		return nil
	}
	return conn.BeginTxFunc(ctx, txOptions, f)
}

func (db *database) Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error) {
	return db.querier.Exec(ctx, sql, args...)
}

func (db *database) Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error) {
	return db.querier.Query(ctx, sql, args...)
}

func (db *database) QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	return db.querier.QueryRow(ctx, sql, args...)
}

func (db *database) Delete(ctx context.Context, entities ...interface{}) error {
	for _, e := range entities {
		err := db.delete(ctx, e)
		if err != nil {
			return err
		}
	}
	return nil
}

func (db *database) delete(ctx context.Context, entity interface{}) error {
	if entity == nil {
		return nil
	}

	e, err := db.getEntityOf(ctx, entity)
	if err != nil {
		return err
	}
	if e == nil {
		return fmt.Errorf("failed to get entity of %v", entity)
	}

	var pk string
	for _, col := range e.cols {
		if col.primaryKey {
			pk = col.name
			break
		}
	}
	if pk == "" {
		return fmt.Errorf("no primary key found of entity %v", e)
	}
	sql, args := db.queryFromPrototype(entity, e, true, false)
	sql = strings.Replace(sql, "SELECT *", "DELETE", 1)

	db.Debug("delete entity", zap.String("sql", sql), zap.Any("args", args))
	_, err = db.Exec(ctx, sql, args...)
	return err
}

func (db *database) Store(ctx context.Context, entities ...interface{}) error {
	for _, e := range entities {
		err := db.store(ctx, e)
		if err != nil {
			return err
		}
	}

	return nil
}

func (db *database) storeFK(ctx context.Context, fk *entity, fkEntity interface{}) (interface{}, error) {
	err := db.store(ctx, fkEntity)
	if err != nil {
		return nil, err
	}
	rv := reflect.Indirect(reflect.ValueOf(fkEntity))
	if !rv.IsValid() || rv.IsZero() {
		return nil, nil
	}

	for _, fkCol := range fk.cols {
		if fkCol.primaryKey {
			rf := rv.Field(fkCol.index)
			if !rf.IsZero() && rf.Kind() == reflect.Ptr {
				rf = rf.Elem()
			}
			return indirectInterface(rf), nil
		}
	}

	return nil, fmt.Errorf("no foreign key found: %v", fk)
}

func (db *database) store(ctx context.Context, ent interface{}) error {
	e, err := db.getEntityOf(ctx, ent)
	if err != nil {
		return err
	}
	if e == nil {
		return fmt.Errorf("failed to get entity of %v", ent)
	}
	vv := make([]interface{}, len(e.cols))
	rv := reflect.Indirect(reflect.ValueOf(ent))
	if !rv.IsValid() || rv.IsZero() {
		return nil
	}
	for i, col := range e.cols {
		rf := rv.Field(col.index)
		for !rf.IsZero() && (rf.Kind() == reflect.Ptr || rf.Kind() == reflect.Interface) {
			rf = rf.Elem()
		}
		v := indirectInterface(rf)
		if col.foreignKey != nil {
			if isArray(rf.Kind()) {
				rt := rf.Type().Elem()
				if rt.Kind() == reflect.Ptr {
					rt = rt.Elem()
				}
				for _, fkCol := range col.foreignKey.cols {
					if fkCol.primaryKey {
						rt = rawType(reflect.Zero(rt).Field(fkCol.index).Type())
						break
					}
				}
				var rvv reflect.Value
				if rf.Kind() == reflect.Slice {
					rvv = reflect.Zero(reflect.SliceOf(rt))
				} else {
					rvv = reflect.Zero(reflect.ArrayOf(rf.Len(), rt))
				}
				for j := 0; j < rf.Len(); j++ {
					v = rf.Index(j).Interface()
					id, err := db.storeFK(ctx, col.foreignKey, v)
					if err != nil {
						return err
					}
					if rf.Kind() == reflect.Slice {
						rvv = reflect.Append(rvv, reflect.ValueOf(id))
					} else {
						rvv.Index(j).Set(reflect.ValueOf(id))
					}
				}
				vv[i] = rvv.Interface()
			} else {
				vv[i], err = db.storeFK(ctx, col.foreignKey, v)
				if err != nil {
					return err
				}
			}
		} else if isArray(rf.Kind()) {
			rt := rf.Type().Elem()
			if rt.Kind() == reflect.Ptr {
				rt = rt.Elem()
			}
			isStruct := false
			if rt.Kind() == reflect.Struct {
				isStruct = true
				rt = stringType
			}
			var rvv reflect.Value
			if rf.Kind() == reflect.Slice {
				rvv = reflect.Zero(reflect.SliceOf(rt))
			} else {
				rvv = reflect.Zero(reflect.ArrayOf(rf.Len(), rt))
			}
			for j := 0; j < rf.Len(); j++ {
				v = rf.Index(j).Interface()
				var value reflect.Value
				if isStruct {
					s, err := db.jm.Marshal(v)
					if err != nil {
						return err
					}
					value = reflect.ValueOf(s)
				} else {
					value = reflect.ValueOf(v)
				}
				if rf.Kind() == reflect.Slice {
					if value.Kind() == reflect.Ptr {
						value = value.Elem()
					}
					rvv = reflect.Append(rvv, value)
				} else {
					rvv.Index(j).Set(value)
				}
			}
			vv[i] = rvv.Interface()
		} else if rf.Kind() == reflect.Map {
			if col.foreignKey != nil {
				//TODO
			}
			vv[i], err = db.mm.Marshal(v)
			if err != nil {
				return err
			}
		} else if rf.Kind() == reflect.Struct {
			vv[i], err = db.jm.Marshal(v)
			if err != nil {
				return err
			}
		} else {
			vv[i] = v
		}
	}

	upsert := createUpsertSQL(e)
	_, err = db.Exec(ctx, upsert, vv...)
	if err != nil {
		db.Error("failed to store entity", zap.String("upsert", upsert), zap.Any("values", vv), zap.Error(err))
	} else {
		db.Debug("stored entity", zap.String("upsert", upsert), zap.Any("values", vv))
	}

	return err
}

func (db *database) RegisterConcreteTypes(entities ...interface{}) {
	for _, e := range entities {
		if e == nil {
			continue
		}
		v := reflect.Indirect(reflect.ValueOf(e))
		db.implTypes[v.Type().String()] = v
	}
}

func (db *database) getImpl(entity interface{}) *reflect.Value {
	t := rawType(reflect.TypeOf(entity))
	for k, v := range db.implTypes {
		if t.String() == k {
			return &v
		}
	}
	return nil
}

func (db *database) LoadByQuery(ctx context.Context, entitySlice interface{}, query string, args ...interface{}) error {
	slice, sliceType, e, entType, err := db.getTypesOf(ctx, entitySlice)
	if err != nil {
		return err
	}
	return db.loadByQuery(ctx, e, entType, slice, sliceType, query, args...)
}

func (db *database) LoadSingleByQuery(ctx context.Context, entity interface{}, query string, args ...interface{}) error {
	e, err := db.getEntityOf(ctx, entity)
	if err != nil {
		return err
	}
	return db.loadSingleByQuery(ctx, e, entity, db.getImpl(entity), query, args...)
}

// Load loads all prototype matching entities into entitySlice. If entitySlice is not empty, it will be emptied before loading.
func (db *database) Load(ctx context.Context, prototype interface{}, caseSensitive, similar bool, entitySlice interface{}) error {
	if prototype != nil {
		if rawType(reflect.TypeOf(prototype)).Kind() != reflect.Struct {
			return errors.New("only prototype structs are supported")
		}
	}

	slice, sliceType, e, entType, err := db.getTypesOf(ctx, entitySlice)
	if err != nil {
		return err
	}
	query, args := db.queryFromPrototype(prototype, e, caseSensitive, similar)
	return db.loadByQuery(ctx, e, entType, slice, sliceType, query, args...)
}

func (db *database) getTypesOf(ctx context.Context, entitySlicePtr interface{}) (slice reflect.Value, sliceType reflect.Type, e *entity, entType reflect.Type, err error) {
	slicePtr := reflect.ValueOf(entitySlicePtr)
	if slicePtr.Kind() != reflect.Ptr {
		err = errors.New("given entitySlicePtr is not a pointer to a slice or array")
		return
	}

	slice = slicePtr.Elem()
	sliceType = slice.Type()
	if !isArray(sliceType.Kind()) {
		err = errors.New("given entitySlice is not a slice or array")
		return
	}

	entType = sliceType.Elem()
	ent := reflect.New(entType).Elem().Interface()
	e, err = db.getEntityOf(ctx, ent)
	return
}

func (db *database) loadByQuery(ctx context.Context, e *entity, entType reflect.Type, slice reflect.Value, sliceType reflect.Type, query string, args ...interface{}) error {
	rows, err := db.Query(ctx, query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	db.Debug("load entities", zap.String("query", query), zap.Any("values", args))
	var vvv [][]interface{}
	for rows.Next() {
		vv := make([]interface{}, len(e.cols))
		for i := range e.cols {
			vv[i] = new(interface{})
			vv[i] = &vv[i]
		}
		err = rows.Scan(vv...)
		if err != nil {
			return err
		}
		vvv = append(vvv, vv)
	}

	for i, vv := range vvv {
		ent := reflect.New(entType).Interface()
		err = db.query(ctx, vv, e, ent, db.getImpl(ent))
		if err != nil {
			return err
		}
		if i == 0 {
			slice.Set(reflect.Append(reflect.MakeSlice(sliceType, 0, 0), reflect.ValueOf(ent).Elem()))
		} else {
			slice.Set(reflect.Append(slice, reflect.ValueOf(ent).Elem()))
		}
	}

	return nil
}

func (db *database) LoadSingle(ctx context.Context, uidOrPrototype, entity interface{}) error {
	return db.load(ctx, uidOrPrototype, entity, db.getImpl(entity))
}

func (db *database) load(ctx context.Context, uidOrPrototype, entity interface{}, impl *reflect.Value) error {
	e, err := db.getEntityOf(ctx, entity)
	if err != nil {
		return err
	}

	query := ""
	var args []interface{}
	rt := rawType(reflect.TypeOf(uidOrPrototype))
	if rt == nil {
		return nil
	}
	if rt.Kind() == reflect.Struct { // prototype delivered
		query, args = db.queryFromPrototype(uidOrPrototype, e, true, false)
	} else { // ID delivered
		var pk string
		for _, col := range e.cols {
			if col.primaryKey {
				pk = col.name
				break
			}
		}
		if pk == "" {
			return fmt.Errorf("no primary key found of entity %v", e)
		}
		query = fmt.Sprintf("SELECT * FROM %s WHERE %s=$1", e.table, pk)
		args = append(args, uidOrPrototype)
	}
	return db.loadSingleByQuery(ctx, e, entity, impl, query, args...)
}

func (db *database) loadSingleByQuery(ctx context.Context, e *entity, entity interface{}, impl *reflect.Value, query string, args ...interface{}) error {
	db.Debug("load entity", zap.String("query", query), zap.Any("values", args))
	row := db.QueryRow(ctx, query, args...)
	vv := make([]interface{}, len(e.cols))
	for i := range e.cols {
		vv[i] = new(interface{})
		vv[i] = &vv[i]
	}
	err := row.Scan(vv...)
	if err != nil {
		return err
	}
	return db.query(ctx, vv, e, entity, impl)
}

func (db *database) queryFromPrototype(prototype interface{}, e *entity, caseSensitive, similar bool) (string, []interface{}) {
	if prototype == nil {
		query := fmt.Sprintf("SELECT * FROM %s", e.table)
		return query, nil
	}

	var args []interface{}
	rv := reflect.Indirect(reflect.ValueOf(prototype))
	if !rv.IsValid() || rv.IsZero() {
		query := fmt.Sprintf("SELECT * FROM %s", e.table)
		return query, nil
	}
	var constraints []string
	for _, col := range e.cols {
		rf := rv.Field(col.index)
		if col.primaryKey && rf.IsValid() && !rf.IsZero() {
			constraints = append(constraints, fmt.Sprintf("%s=$1", col.name))
			args = append(args, rf.Interface())
			break
		}
	}
	if len(constraints) == 0 {
		idx := 1
		for _, col := range e.cols {
			rf := rv.Field(col.index)
			if col.primaryKey || !rf.IsValid() || rf.IsZero() {
				continue
			}
			rv2 := reflect.Indirect(reflect.ValueOf(rf.Interface()))
			if rv2.IsValid() && rv2.IsZero() {
				continue
			}
			switch rv2.Kind() {
			case reflect.Slice, reflect.Array:
				placeholders := make([]string, rv2.Len())
				for k := 0; k < rv2.Len(); k++ {
					placeholders[k] = fmt.Sprintf("$%d=ANY(%s)", idx, col.name)
					idx++

					rve := rv2.Index(k)
					if rve.Kind() == reflect.Ptr {
						rve = rve.Elem()
					}
					if col.foreignKey != nil {
						for _, fkCol := range col.foreignKey.cols {
							if fkCol.primaryKey {
								rve = rve.Field(fkCol.index)
								if rve.Kind() == reflect.Ptr {
									rve = rve.Elem()
								}
								break
							}
						}
					}
					args = append(args, rve.Interface())
				}
				constraints = append(constraints, strings.Join(placeholders, ","))
			default:
				rf2 := rf
				if col.foreignKey != nil {
					for _, fkc := range col.foreignKey.cols {
						if fkc.primaryKey {
							rf2 = rv2.Field(fkc.index)
							break
						}
					}
				}

				s, ok := reflect.Indirect(rf2).Interface().(string)
				if !ok {
					args = append(args, rf2.Interface())
					if caseSensitive {
						constraints = append(constraints, fmt.Sprintf("%s=$%d", col.name, idx))
					} else {
						constraints = append(constraints, fmt.Sprintf("LOWER(%s)=LOWER($%d)", col.name, idx))
					}
				} else {
					op := "="
					if similar {
						op = " SIMILAR TO "
						args = append(args, "%"+s+"%")
					} else {
						args = append(args, s)
					}
					if caseSensitive {
						constraints = append(constraints, fmt.Sprintf("%s%s$%d", col.name, op, idx))
					} else {
						constraints = append(constraints, fmt.Sprintf("LOWER(%s)%sLOWER($%d)", col.name, op, idx))
					}
				}
				idx++
			}
		}
	}

	if len(constraints) == 0 {
		query := fmt.Sprintf("SELECT * FROM %s", e.table)
		return query, nil
	}

	query := fmt.Sprintf("SELECT * FROM %s WHERE %s", e.table, strings.Join(constraints, " AND "))
	return query, args
}

func (db *database) query(ctx context.Context, vv []interface{}, e *entity, entity interface{}, impl *reflect.Value) error {
	var err error
	rv := reflect.Indirect(reflect.ValueOf(entity))
	for i, col := range e.cols {
		rf := rv.Field(col.index)
		if col.foreignKey != nil {
			if vv[i] == nil {
				continue
			}
			switch vv[i].(type) {
			case pgtype.TextArray, pgtype.Int2Array, pgtype.Int4Array, pgtype.Int8Array, pgtype.BoolArray, pgtype.Float4Array, pgtype.Float8Array, pgtype.ByteaArray, pgtype.TimestampArray, pgtype.TimestamptzArray, pgtype.ACLItemArray, pgtype.BPCharArray, pgtype.CIDRArray, pgtype.DateArray, pgtype.EnumArray, pgtype.HstoreArray, pgtype.InetArray, pgtype.JSONBArray, pgtype.MacaddrArray, pgtype.NumericArray, pgtype.TstzrangeArray, pgtype.UntypedTextArray, pgtype.UUIDArray, pgtype.VarcharArray:
				aa, err := db.fetchFromArray(rf, vv[i], func(value reflect.Value, rawType reflect.Type) (*reflect.Value, error) {
					fkrv := db.newPtr(col.foreignKey.table)
					if impl != nil {
						info := impl.Field(col.index)
						err = db.load(ctx, value.Interface(), fkrv.Interface(), &info)
					} else {
						err = db.load(ctx, value.Interface(), fkrv.Interface(), nil)
					}
					if err != nil {
						return nil, err
					}
					if fkrv.Kind() == reflect.Ptr {
						value = fkrv.Elem()
					} else {
						value = fkrv
					}
					return &value, nil
				})
				if err != nil {
					return err
				}
				if aa == nil {
					continue
				}
				if rf.Kind() == reflect.Ptr {
					ptr := reflect.New(rf.Type().Elem())
					ptr.Elem().Set(*aa)
					rf.Set(ptr)
				} else {
					rf.Set(*aa)
				}
			default:
				fkrv := db.newPtr(col.foreignKey.table)
				if impl != nil {
					info := impl.Field(col.index)
					err = db.load(ctx, vv[i], fkrv.Interface(), &info)
				} else {
					err = db.load(ctx, vv[i], fkrv.Interface(), nil)
				}
				if err != nil {
					return err
				}
				if rf.Kind() != reflect.Ptr {
					fkrv = fkrv.Elem()
				}
				rf.Set(fkrv.Convert(rf.Type()))
			}
		} else {
			rv2 := reflect.Indirect(reflect.ValueOf(vv[i]))
			if !rv2.IsValid() {
				continue
			}
			if rv2.IsZero() {
				if rf.Kind() == reflect.Ptr {
					rf.Set(reflect.New(rf.Type().Elem()))
				}
				continue
			}
			switch rv2.Interface().(type) {
			case pgtype.TextArray, pgtype.Int2Array, pgtype.Int4Array, pgtype.Int8Array, pgtype.BoolArray, pgtype.Float4Array, pgtype.Float8Array, pgtype.ByteaArray, pgtype.TimestampArray, pgtype.TimestamptzArray, pgtype.ACLItemArray, pgtype.BPCharArray, pgtype.CIDRArray, pgtype.DateArray, pgtype.EnumArray, pgtype.HstoreArray, pgtype.InetArray, pgtype.JSONBArray, pgtype.MacaddrArray, pgtype.NumericArray, pgtype.TstzrangeArray, pgtype.UntypedTextArray, pgtype.UUIDArray, pgtype.VarcharArray:
				aa, err := db.fetchFromArray(rf, rv2.Interface(), func(value reflect.Value, rawType reflect.Type) (*reflect.Value, error) {
					v, ok := value.Interface().(string)
					if ok && rawType != stringType {
						ptr := reflect.New(rawType)
						err = db.jm.Unmarshal(v, ptr.Interface())
						if err != nil {
							value = value.Convert(rawType)
						} else {
							value = ptr.Elem()
						}
					} else {
						value = value.Convert(rawType)
					}
					return &value, nil
				})
				if err != nil {
					return err
				}
				if aa == nil {
					continue
				}
				rv2 = *aa
			}

			rf2 := rf
			if rf.Kind() == reflect.Ptr {
				rf2.Set(reflect.New(rf2.Type().Elem()))
				rf2 = rf2.Elem()
			}
			switch rf2.Kind() {
			case reflect.Map:
				ptr := reflect.New(rf2.Type())
				ptr.Elem().Set(reflect.MakeMap(rf2.Type()))
				err = db.mm.Unmarshal(rv2.Interface().(string), ptr.Interface())
				if err != nil {
					return err
				}
				rf2.Set(ptr.Elem())
			default:
				if rv2.Type() == stringType && rf2.Kind() == reflect.Struct {
					ptr := reflect.New(rf2.Type())
					err = db.jm.Unmarshal(rv2.Interface().(string), ptr.Interface())
					if err != nil {
						return err
					}
					rf2.Set(ptr.Elem())
				} else if rv2.Type() == stringType && rf2.Kind() == reflect.Interface {
					if impl == nil || impl.Field(col.index).IsZero() {
						return fmt.Errorf("unable to determine concrete type of %s{%s %s}. Make sure to provide this information via db.RegisterConcreteTypes", e.table, rawType(reflect.TypeOf(entity)).Field(col.index).Name, rf2.Type())
					}
					ptr := reflect.New(reflect.Indirect(impl.Field(col.index).Elem()).Type())
					err = db.jm.Unmarshal(rv2.Interface().(string), ptr.Interface())
					if err != nil {
						return err
					}
					rf2.Set(ptr)
				} else {
					rf2.Set(rv2.Convert(rf2.Type()))
				}
			}
		}
	}

	return nil
}

func (db *database) fetchFromArray(rf reflect.Value, t interface{}, fn func(reflect.Value, reflect.Type) (*reflect.Value, error)) (*reflect.Value, error) {
	dimensions := reflect.ValueOf(t).FieldByName("Dimensions").Interface().([]pgtype.ArrayDimension)
	elements := reflect.ValueOf(t).FieldByName("Elements")

	dims := len(dimensions)
	if dims == 0 {
		return nil, nil
	}
	rt := rf.Type()

	type helper struct {
		Index   int
		length  int
		isPtr   bool
		typ     reflect.Type
		rawType reflect.Type
	}

	aa := make([]reflect.Value, dims)
	hh := make([]*helper, dims)
	for d := 0; d < dims; d++ {
		if d > 0 {
			aa[d-1] = reflect.Zero(reflect.SliceOf(rt))
		}
		if rt.Kind() == reflect.Ptr {
			rt = rt.Elem()
		}
		rt = rt.Elem()
		hh[d] = &helper{
			Index:   0,
			length:  int(dimensions[d].Length),
			typ:     rt,
			rawType: rt,
		}
		if rt.Kind() == reflect.Ptr {
			rt = rt.Elem()
			hh[d].isPtr = true
			hh[d].rawType = rt
		}
	}

	idx := 0
	for d := dims - 1; d >= 0; d-- {
		h := hh[d]
		if d == dims-1 {
			if h.isPtr {
				aa[d] = reflect.Zero(reflect.SliceOf(h.typ))
			} else {
				aa[d] = reflect.Zero(reflect.SliceOf(h.rawType))
			}
			for k := 0; k < h.length; k++ {
				rv := reflect.ValueOf(elements.Index(idx).Addr().Interface().(pgtype.Value).Get())
				x, err := fn(rv, h.rawType)
				if err != nil {
					return nil, err
				}
				rv = *x
				idx++
				if h.isPtr {
					ptr := reflect.New(h.rawType)
					ptr.Elem().Set(rv)
					aa[d] = reflect.Append(aa[d], ptr)
				} else {
					aa[d] = reflect.Append(aa[d], rv)
				}
			}
		}

		if d > 0 {
			aa[d-1] = reflect.Append(aa[d-1], aa[d])
			h.Index++
			if h.Index < hh[d-1].length {
				d = dims
			}
		}
	}

	return &aa[0], nil
}

func (db *database) Complete(ctx context.Context, prototype interface{}) error {
	return db.LoadSingle(ctx, prototype, prototype)
}

func (db *database) Close() error {
	if db.cancel != nil {
		defer db.cancel()
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if db.cfg.DropTables {
		for _, e := range db.reg {
			err := db.dropTable(ctx, db.newInstance(e.table))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (db *database) dropTables(ctx context.Context) error {
	for _, e := range db.reg {
		err := db.dropTable(ctx, db.newInstance(e.table))
		if err != nil {
			return err
		}
	}
	return nil
}

// createSchema creates the schema of the given entity if not already present
func (db *database) createSchema(ctx context.Context, entity *entity) error {
	ddl := db.createSchemaDDL(entity.schema)
	_, err := db.Exec(ctx, ddl)
	if err != nil {
		db.Error("failed to create schema", zap.String("ddl", ddl))
		return err
	}
	db.Debug("created schema", zap.String("ddl", ddl))
	return nil
}

// createTable creates the table of the given entity if not already present
func (db *database) createTable(ctx context.Context, entity interface{}) error {
	e, err := db.getEntityOf(ctx, entity)
	if err != nil {
		return err
	}
	err = db.createSchema(ctx, e)
	if err != nil {
		return err
	}
	for _, col := range e.cols {
		if col.foreignKey != nil {
			err = db.createTable(ctx, db.newInstance(col.foreignKey.table))
			if err != nil {
				return err
			}
		}
	}
	ddl, err := db.createTableDDL(e)
	if err != nil {
		db.Error("failed to create table", zap.Error(err))
		return err
	}
	_, err = db.Exec(ctx, ddl)
	if err != nil {
		db.Error("failed to create table", zap.String("ddl", ddl))
		return err
	}
	db.Debug("created table", zap.String("ddl", ddl))
	return nil
}

// dropTable drops the table of the given entity if present
func (db *database) dropTable(ctx context.Context, entity interface{}) error {
	e, err := db.getEntityOf(ctx, entity)
	if err != nil {
		return err
	}
	ddl := db.dropTableDDL(e)
	_, err = db.Exec(ctx, ddl)
	if err != nil {
		db.Error("failed to drop table", zap.String("ddl", ddl))
		return err
	}
	db.Debug("dropped table", zap.String("ddl", ddl))
	return nil
}

func (db *database) register(ctx context.Context, ent interface{}) (*entity, bool, error) {
	if ent == nil {
		return nil, false, nil
	}
	rt := rawType(reflect.TypeOf(ent))
	if rt.Kind() != reflect.Struct {
		return nil, false, nil
	}
	schema, table := "", ""
	pp := strings.Split(rt.String(), ".")
	if len(pp) > 1 {
		schema = db.toDBName(pp[0])
		table = strings.Join(pp[1:], ".")
	} else {
		table = pp[0]
	}
	table = db.toDBName(table)
	if schema != "" {
		table = fmt.Sprintf("%s.%s", schema, table)
	}
	e, ok := db.reg[table]
	if ok {
		return e, true, nil
	}
	e = &entity{
		typ:    rt,
		schema: schema,
		table:  table,
	}
	var err error
	hasPK := false
	for i := 0; i < rt.NumField(); i++ {
		rf := rt.Field(i)
		if unicode.IsLower(rune(rf.Name[0])) {
			continue
		}
		t := getTag(rf)
		if t == nil {
			continue
		}
		colName := db.toDBName(t.col)
		col := &column{
			index:      i,
			name:       colName,
			typ:        mapDBType(rf),
			primaryKey: t.primaryKey,
		}
		if col.primaryKey {
			hasPK = true
		}
		fke, ok, err := db.register(ctx, reflect.Zero(rawType(rf.Type)).Interface())
		if err != nil {
			return nil, false, err
		}
		if ok {
			col.foreignKey = fke
			for _, fkCol := range fke.cols {
				if fkCol.primaryKey {
					dim := ""
					idx := strings.Index(col.typ, "[")
					if idx > 0 {
						dim = col.typ[idx:]
					}
					col.typ = fkCol.typ + dim
					break
				}
			}
		}
		e.cols = append(e.cols, col)
	}
	if !hasPK {
		return nil, false, nil
	}

	m := make(map[string]bool)
	pk := false
	for _, col := range e.cols {
		_, ok = m[col.name]
		if ok {
			return nil, false, fmt.Errorf("at least two columns with same name: %s", col.name)
		}
		if col.primaryKey {
			if pk {
				return nil, false, fmt.Errorf("more than one primary key: %s", col.name)
			}
			pk = true
		}
		m[col.name] = true
	}

	db.reg[e.table] = e
	err = db.createTable(ctx, ent)
	if err != nil {
		return nil, false, err
	}
	return e, true, nil
}

func (db *database) getEntityOf(ctx context.Context, entity interface{}) (*entity, error) {
	rt := rawType(reflect.TypeOf(entity))
	name := rt.String()
	var err error
	e, ok := db.reg[name]
	if !ok {
		e, _, err = db.register(ctx, reflect.New(rt).Elem().Interface())
		if err != nil {
			return nil, err
		}
		if e == nil {
			return nil, fmt.Errorf("entity %s has no primary key", name)
		}
	}
	return e, nil
}

func (db *database) newInstance(name string) interface{} {
	return db.newPtr(name).Interface()
}

func (db *database) newPtr(name string) reflect.Value {
	return reflect.New(db.reg[name].typ)
}

func rawType(typ reflect.Type) reflect.Type {
	if typ == nil {
		return nil
	}
	for typ.Kind() == reflect.Ptr || isArray(typ.Kind()) {
		typ = typ.Elem()
	}
	return typ
}

func indirectInterface(value reflect.Value) interface{} {
	if value.IsZero() {
		return value.Interface()
	}
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}
	return value.Interface()
}

func isArray(kind reflect.Kind) bool {
	return kind == reflect.Array || kind == reflect.Slice
}

var baseTypes = []reflect.Type{
	reflect.TypeOf(0),
	reflect.TypeOf(int8(0)),
	reflect.TypeOf(int16(0)),
	reflect.TypeOf(int32(0)),
	reflect.TypeOf(int64(0)),
	reflect.TypeOf(uint(0)),
	reflect.TypeOf(uint8(0)),
	reflect.TypeOf(uint16(0)),
	reflect.TypeOf(uint32(0)),
	reflect.TypeOf(uint64(0)),
	reflect.TypeOf(float32(0)),
	reflect.TypeOf(float64(0)),
	stringType,
	reflect.TypeOf('c'),
	reflect.TypeOf(rune(2)),
	reflect.TypeOf(byte(2)),
	reflect.TypeOf(false),
}

func isBaseType(typ reflect.Type) bool {
	for _, rt := range baseTypes {
		if typ == rt {
			return true
		}
	}
	return false
}

func getTag(field reflect.StructField) *tag {
	snakeCaseName := toSnakeCase(field.Name)
	t := new(tag)
	dbTag, ok := field.Tag.Lookup("db")
	if !ok {
		t.col = snakeCaseName
	} else {
		if dbTag == "pk" && snakeCaseName != "pk" {
			t.col = snakeCaseName
			t.primaryKey = true
			return t
		}
		t.col = dbTag
	}
	if dbTag == "-" {
		return nil
	}
	t.primaryKey = strings.EqualFold(t.col, "uuid") || strings.EqualFold(t.col, "uid") || strings.EqualFold(t.col, "id")
	return t
}

var (
	matchFirstCap = regexp.MustCompile("([A-Z])([A-Z][a-z])")
	matchAllCap   = regexp.MustCompile("([a-z0-9])([A-Z])")
)

func toSnakeCase(s string) string {
	sc := matchFirstCap.ReplaceAllString(s, "${1}_${2}")
	sc = matchAllCap.ReplaceAllString(sc, "${1}_${2}")
	sc = strings.ReplaceAll(sc, "-", "_")
	return strings.ToLower(sc)
}

func mapDBType(field reflect.StructField) string {
	typ := ""
	name := field.Type.String()
	if !isBaseType(field.Type) {
		for _, t := range baseTypes {
			if field.Type.AssignableTo(t) || field.Type.ConvertibleTo(t) {
				name = t.String()
				break
			}
		}
	}
	dim := 0
	for strings.HasPrefix(name, "[]") || strings.HasPrefix(name, "*") {
		if strings.HasPrefix(name, "[]") {
			name = name[2:]
			dim++
		}
		if strings.HasPrefix(name, "*") {
			name = name[1:]
		}
	}
	switch name {
	case "bool":
		typ = "boolean"
	case "int", "int8", "int16", "int32", "uint", "uint8", "uint16", "uint32":
		typ = "integer"
	case "int64", "uint64":
		typ = "bigint"
	case "float", "float32":
		typ = "real"
	case "float64":
		typ = "double precision"
	case "rune":
		typ = "character"
	case "byte":
		if dim == 0 {
			typ = "character"
		} else {
			typ = "bytea"
			dim--
		}
	case "time.Time":
		typ = "timestamp"
	default:
		typ = "text"
	}
	for i := 0; i < dim; i++ {
		typ += "[]"
	}
	return typ
}
