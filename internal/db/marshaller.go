package db

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
)

type Marshaller interface {
	Marshal(in interface{}) (string, error)
	Unmarshal(in string, out interface{}) error
}

var stringType = reflect.TypeOf("")

type JsonMarshaller struct {
}

func (m *JsonMarshaller) Marshal(in interface{}) (string, error) {
	bb, err := json.Marshal(in)
	if err != nil {
		return "", err
	}
	return string(bb), nil
}

func (m *JsonMarshaller) Unmarshal(in string, out interface{}) error {
	return json.Unmarshal([]byte(in), out)
}

type MapMarshaller struct {
}

func (m *MapMarshaller) Marshal(in interface{}) (string, error) {
	v := reflect.ValueOf(in)
	if v.Kind() == reflect.Ptr {
		if v.IsNil() {
			return "", errors.New("nil pointer provided")
		}
		v = v.Elem()
	}
	if v.Kind() != reflect.Map {
		return "", fmt.Errorf("no map provided: %v", in)
	}
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	dbMap := make(map[string]interface{})
	for _, key := range v.MapKeys() {
		value := v.MapIndex(key)
		if key.Kind() == reflect.Ptr {
			key = key.Elem()
		}
		k := key.Convert(stringType).Interface().(string)
		if value.Kind() == reflect.Ptr {
			value = value.Elem()
		}
		dbMap[k] = value.Interface()
	}
	bb, err := json.Marshal(dbMap)
	if err != nil {
		return "", err
	}
	return string(bb), nil
}

func (m *MapMarshaller) Unmarshal(in string, out interface{}) error {
	v := reflect.ValueOf(out)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Map {
		return fmt.Errorf("target is no pointer to %s", reflect.TypeOf(out).String())
	}

	dbMap := new(map[string]interface{})
	err := json.Unmarshal([]byte(in), dbMap)
	if err != nil {
		return err
	}

	v = v.Elem()
	keyType := v.Type().Key()
	valType := v.Type().Elem()
	v.Set(reflect.MakeMapWithSize(reflect.MapOf(keyType, valType), len(*dbMap)))
	keyPtr := false
	valPtr := false
	if keyType.Kind() == reflect.Ptr {
		keyType = keyType.Elem()
		keyPtr = true
	}
	if valType.Kind() == reflect.Ptr {
		valType = valType.Elem()
		valPtr = true
	}
	for key, val := range *dbMap {
		var kk, vv reflect.Value
		if keyPtr {
			kk = reflect.New(keyType)
			kk.Elem().Set(reflect.ValueOf(key).Convert(keyType))
		} else {
			kk = reflect.ValueOf(key).Convert(keyType)
		}
		if valPtr {
			vv = reflect.New(valType)
			vv.Elem().Set(reflect.ValueOf(val).Convert(valType))
		} else {
			vv = reflect.ValueOf(val).Convert(valType)
		}
		v.SetMapIndex(kk, vv)
	}
	return nil
}
