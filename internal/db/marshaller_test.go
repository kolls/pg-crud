package db

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestMapMarshaller(t *testing.T) {
	// given
	m := map[string]string{"a": "A", "b": "B"}
	mm := MapMarshaller{}

	// when
	s, err := mm.Marshal(m)

	// then
	require.Nil(t, err)
	require.Equal(t, "{\"a\":\"A\",\"b\":\"B\"}", s)

	// when
	out := new(map[string]string)
	err = mm.Unmarshal(s, out)

	// then
	require.Nil(t, err)
	require.Equal(t, m, *out)
}

func TestMapPtrMarshaller(t *testing.T) {
	// given
	m := map[*string]*string{stringPtr("a"): stringPtr("A"), stringPtr("b"): stringPtr("B")}
	mm := MapMarshaller{}

	// when
	s, err := mm.Marshal(&m)

	// then
	require.Nil(t, err)
	require.Equal(t, "{\"a\":\"A\",\"b\":\"B\"}", s)

	// when
	out := new(map[*string]*string)
	err = mm.Unmarshal(s, out)

	// then
	require.Nil(t, err)
	require.Equal(t, len(m), len(*out))
	for k, v := range *out {
		isEqual := false
		for ek, ev := range m {
			if *ek == *k {
				require.Equal(t, *ev, *v)
				isEqual = true
				break
			}
		}
		require.True(t, isEqual)
	}
}

func stringPtr(s string) *string {
	return &s
}
