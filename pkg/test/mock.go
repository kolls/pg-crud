package test

import (
	"context"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jinzhu/copier"
	"github.com/stretchr/testify/require"
	"gitlab.com/kolls/pg-crud/internal/db"
	"gitlab.com/kolls/pg-crud/pkg/config"
	"gitlab.com/kolls/pg-crud/pkg/pg"
	"go.uber.org/zap"
	"reflect"
	"testing"
)

type (
	Exec     func(sql string, args ...interface{}) (pgconn.CommandTag, error)
	Query    func(sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow func(sql string, args ...interface{}) []interface{}
)

type Config struct {
	caseSensitive              bool
	replaceDotsWithUnderscores bool
	dropTables                 bool

	exec     Exec
	query    Query
	queryRow QueryRow

	expectations []*exp
}

func Default() *Config {
	cfg := config.Default()
	return &Config{
		caseSensitive:              cfg.CaseSensitive,
		replaceDotsWithUnderscores: cfg.ReplaceDotsWithUnderscores,
		dropTables:                 cfg.DropTables,
	}
}

type Option func(*Config)

type rowScanner struct {
	dest []interface{}
}

func (r *rowScanner) Scan(dest ...interface{}) error {
	for i, d := range r.dest {
		reflect.ValueOf(dest[i]).Elem().Set(reflect.ValueOf(d))
	}
	return nil
}

type MockedDB struct {
	t   *testing.T
	cfg *Config
	rs  *rowScanner
	pg.Database
}

func MockDB(t *testing.T, logger *zap.Logger, opts ...Option) *MockedDB {
	cfg := Default()
	for _, opt := range opts {
		opt(cfg)
	}
	mdb := &MockedDB{
		t:   t,
		cfg: cfg,
		rs:  new(rowScanner),
	}
	mdb.Database = db.New(mdb, logger,
		config.SetCaseSensitive(cfg.caseSensitive),
		config.SetReplaceDotsWithUnderscores(cfg.replaceDotsWithUnderscores),
		config.SetDropTables(cfg.dropTables),
	)
	return mdb
}

func CaseSensitive(caseSensitive bool) Option {
	return func(cfg *Config) {
		cfg.caseSensitive = caseSensitive
	}
}

func ReplaceDotsWithUnderscores(replaceDotsWithUnderscores bool) Option {
	return func(cfg *Config) {
		cfg.replaceDotsWithUnderscores = replaceDotsWithUnderscores
	}
}

func OnExec(exec Exec) Option {
	return func(cfg *Config) {
		cfg.exec = exec
	}
}

func OnQuery(query Query) Option {
	return func(cfg *Config) {
		cfg.query = query
	}
}

func OnQueryRow(queryRow QueryRow) Option {
	return func(cfg *Config) {
		cfg.queryRow = queryRow
	}
}

func (db *MockedDB) Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error) {
	if db.cfg.exec != nil {

	}
	return db.Database.Exec(ctx, sql, args...)
}

func (db *MockedDB) Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error) {
	if db.cfg.query != nil {

	}
	return db.Database.Query(ctx, sql, args...)
}

func (db *MockedDB) QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	if db.cfg.queryRow != nil {
		db.rs.dest = db.cfg.queryRow(sql, args)
		return db.rs
	}
	for _, e := range db.cfg.expectations {
		entity := reflect.ValueOf(e.returnValue).Interface()
		db.rs.dest = []interface{}{entity}
	}
	return db.Database.QueryRow(ctx, sql, args...)
}

func (db *MockedDB) Store(ctx context.Context, entities ...interface{}) error {
	for _, ent := range entities {
		for _, e := range db.cfg.expectations {
			entity := reflect.ValueOf(ent).Elem().Interface()
			if reflect.DeepEqual(entity, e.storeEntity) {
				e.matched = true
			}
		}
	}
	return db.Database.Store(ctx, entities...)
}

func (db *MockedDB) AssertExpectations() {
	for _, e := range db.cfg.expectations {
		e.Assert()
	}
}

func Expect(t *testing.T) *exp {
	return &exp{
		t: t,
	}
}

type exp struct {
	t           *testing.T
	storeEntity interface{}
	returnValue interface{}
	matched     bool
}

func (e *exp) Store(entity interface{}) *mock {
	from := reflect.ValueOf(entity).Elem().Interface()
	to := reflect.New(reflect.TypeOf(entity).Elem()).Elem()
	err := copier.CopyWithOption(&to, &from, copier.Option{IgnoreEmpty: false, DeepCopy: true})
	require.Nil(e.t, err)
	e.storeEntity = to
	return &mock{
		e: e,
	}
}

func (e *exp) Option() Option {
	return func(cfg *Config) {
		cfg.expectations = append(cfg.expectations, e)
	}
}

func (e *exp) Assert() {
	require.True(e.t, e.matched)
}

type mock struct {
	e *exp
}

func (m *mock) Return(value interface{}) Option {
	m.e.returnValue = value
	return m.Option()
}

func (m *mock) Option() Option {
	return m.e.Option()
}
