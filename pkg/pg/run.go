package pg

import (
	"context"
	"fmt"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"gitlab.com/kolls/pg-crud/pkg/config"
	"go.uber.org/zap"
	"os"
	usr "os/user"
	"strconv"
)

type DB struct {
	Database
	pool      *dockertest.Pool
	container *docker.Container
	Shutdown  func() //closes the DB and deletes the container
}

func Run(dataDir, dbName, user, password string, port int, containerName string, logger *zap.Logger, opts ...config.Option) (*DB, error) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		return nil, err
	}

	cc, err := pool.Client.ListContainers(docker.ListContainersOptions{
		All: true,
		Filters: map[string][]string{
			"name": {containerName},
		},
	})
	if err != nil {
		return nil, err
	}

	var c *docker.Container

	if len(cc) == 1 {
		c, err = pool.Client.InspectContainer(cc[0].ID)
		if err != nil {
			return nil, err
		}
	} else {
		err = os.MkdirAll(dataDir, 0755)
		if err != nil {
			return nil, err
		}

		u, err := usr.Current()
		if err != nil {
			return nil, err
		}
		cco := docker.CreateContainerOptions{
			Name: containerName,
			Config: &docker.Config{
				Image: "postgres:13.1",
				User:  fmt.Sprintf("%s:%s", u.Uid, u.Gid),
				Env: []string{
					"POSTGRES_USER=" + user,
					"POSTGRES_PASSWORD=" + password,
					"POSTGRES_DB=" + dbName,
					"POSTGRES_INITDB_ARGS=--encoding=UTF8",
				},
				ExposedPorts: map[docker.Port]struct{}{"5432": {}},
			},
			HostConfig: &docker.HostConfig{
				GroupAdd: []string{u.Gid},
				Mounts: []docker.HostMount{
					{
						Source: dataDir,
						Target: "/var/lib/postgresql/data",
						Type:   "bind",
					},
					{
						Source:   "/etc/passwd",
						Target:   "/etc/passwd",
						Type:     "bind",
						ReadOnly: true,
					},
				},
				PortBindings: map[docker.Port][]docker.PortBinding{
					"5432": {
						{HostIP: "0.0.0.0", HostPort: strconv.Itoa(port)},
					},
				},
			},
		}
		c, err = pool.Client.CreateContainer(cco)
		if err != nil {
			return nil, err
		}
	}

	if !c.State.Running {
		err = pool.Client.StartContainer(c.ID, nil)
		if err != nil {
			return nil, err
		}
	}

	ctx := context.Background()
	url := fmt.Sprintf("postgres://%s:%s@localhost:%d/%s?sslmode=disable", user, password, port, dbName)
	var database Database
	if logger == nil {
		logger = zap.NewNop()
	}
	err = pool.Retry(func() error {
		database, err = Connect(ctx, url, logger, opts...)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	db := &DB{
		Database:  database,
		pool:      pool,
		container: c,
	}

	db.Shutdown = func() {
		_ = db.Close()

		_ = db.pool.Client.RemoveContainer(docker.RemoveContainerOptions{
			ID:            db.container.ID,
			Force:         true,
			RemoveVolumes: false,
		})
	}

	return db, nil
}
