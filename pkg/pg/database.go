package pg

import (
	"context"
	"github.com/jackc/pgtype/pgxtype"
	"github.com/jackc/pgx/v4"
	"gitlab.com/kolls/pg-crud/internal/db"
	"gitlab.com/kolls/pg-crud/pkg/config"
	"go.uber.org/zap"
)

type Database interface {
	pgxtype.Querier

	Store(ctx context.Context, entities ...interface{}) error
	Load(ctx context.Context, prototype interface{}, caseSensitive, similar bool, entitySlice interface{}) error
	LoadSingle(ctx context.Context, uidOrPrototype, entity interface{}) error
	Complete(ctx context.Context, prototype interface{}) error
	Delete(ctx context.Context, entities ...interface{}) error

	LoadByQuery(ctx context.Context, entitySlice interface{}, query string, args ...interface{}) error
	LoadSingleByQuery(ctx context.Context, entity interface{}, query string, args ...interface{}) error

	Begin(ctx context.Context) (pgx.Tx, error)
	BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error)
	BeginFunc(ctx context.Context, f func(pgx.Tx) error) error
	BeginTxFunc(ctx context.Context, txOptions pgx.TxOptions, f func(pgx.Tx) error) error

	Close() error

	RegisterConcreteTypes(entities ...interface{})
}

func Connect(ctx context.Context, url string, logger *zap.Logger, opts ...config.Option) (Database, error) {
	return db.Connect(ctx, url, logger, opts...)
}
