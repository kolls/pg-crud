package config

type Config struct {
	CaseSensitive              bool
	ReplaceDotsWithUnderscores bool
	DropTables                 bool
}

func Default() *Config {
	return &Config{
		ReplaceDotsWithUnderscores: true,
	}
}

type Option func(*Config)

func CaseSensitive() Option {
	return func(cfg *Config) {
		cfg.CaseSensitive = true
	}
}

func SetCaseSensitive(flag bool) Option {
	return func(cfg *Config) {
		cfg.CaseSensitive = flag
	}
}

func DoNotReplaceDotsWithUnderscores() Option {
	return func(cfg *Config) {
		cfg.ReplaceDotsWithUnderscores = false
	}
}

func SetReplaceDotsWithUnderscores(flag bool) Option {
	return func(cfg *Config) {
		cfg.ReplaceDotsWithUnderscores = flag
	}
}

func DropTables() Option {
	return func(cfg *Config) {
		cfg.DropTables = true
	}
}

func SetDropTables(flag bool) Option {
	return func(cfg *Config) {
		cfg.DropTables = flag
	}
}
